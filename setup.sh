#!/usr/bin/env bash

PROJECT_DIR="$PWD" # Can't use dirname etc. here because $0 is not always our file
RUST_IMPL="${PROJECT_DIR}/rust"
PYTHON_IMPL="${PROJECT_DIR}/python"

install() {
	# Create virtualenv
	virtualenv ${PROJECT_DIR}/venv -p python3

	# Enter
	source ${PROJECT_DIR}/venv/bin/activate

	# Install rust tooling
	pip3 install maturin

	# Build rust module
	{
		cd ${RUST_IMPL}
		maturin develop --release
	}
}

env() {
	source venv/bin/activate
	export PYTHONPATH=${PYTHON_IMPL}:$PYTHONPATH
}

# Run function given to script (Was to lazy to write a proper command line parser)
if [ $1 == "install" ] || [ $1 == "env" ]; then
	$1
else
	echo "Possible arguments: install, env"
fi
