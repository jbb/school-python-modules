#!/usr/bin/env python3

# For generating the list
import random

# for taking time
import time

# for managing file
import os

# Sort implementations
import insertionsort_rs
import insertionsort_py



"""
# Generate numbers from 0 to @param max
"""
def generate_list(len: int) -> list:
    return [random.randint(0, 10000) for iter in range(int(len))]

def take_time(function, *kwargs) -> float:
    before = time.time()

    out = function(*kwargs)

    after = time.time()

    return {
        "time": after - before,
        "out": out
    }

def run_test(len: int):
    try:
        csv = open("stats.csv", "r").read()
    except FileNotFoundError:
        csv = ""

    print("Länge", len)

    list = generate_list(len)

    rust = take_time(insertionsort_rs.sort, list)
    python = take_time(insertionsort_py.sort, list)

    assert rust["out"] == python["out"]

    if (not "python" in csv):
        csv += ("python\n")
    if (not "rust" in csv):
        csv += ("rust\n")
    if (not "len" in csv):
        csv += ("len\n")

    for line in csv.split("\n"):
        if ("python" in line):
            csv = csv.replace(line, line + "," + str(python["time"]))

        if ("rust" in line):
            csv = csv.replace(line, line + "," + str(rust["time"]))

        if ("len" in line):
            csv = csv.replace(line, line + "," + str(len))

    csv_out = open("stats.csv", "w")
    csv_out.seek(0)
    csv_out.write(csv)

if os.path.isfile("stats.csv"):
    os.remove("stats.csv")

"""for i in range(1, 5):
    run_test(5 * 10 ** i)
"""
run_test(50)
run_test(500)
run_test(5000)
run_test(10000)
run_test(15000)
run_test(20000)
run_test(25000)
run_test(30000)
