# Implementation based on following pseudocode
"""
INSERTIONSORT(A)
1 for i = 1 to (Länge(A)-1) do
2      einzusortierender_wert = A[i]
3      j = i
4      while (j > 0) and (A[j-1] > einzusortierender_wert) do
5           A[j] = A[j - 1]
6           j = j − 1
7      end while
8      A[j] = einzusortierender_wert
9 end for
*/
"""
def sort(list: list):
    for i in range(len(list)):
        value = list[i]
        j = i
        while j > 0 and list[j - 1] > value:
            list[j] = list[j - 1]
            j = j -1

        list[j] = value

    return list
