
extern crate pyo3;

use pyo3::prelude::{pyfunction, pymodule, PyModule, PyResult, Python, ToPyObject, *};
use pyo3::PyObject;
use pyo3::types::{PyList, PyAny, PyLong};
use pyo3::wrap_pyfunction;
use std::convert::TryFrom;


// Implementation based on following pseudocode
/*
INSERTIONSORT(A)
1 for i = 1 to (Länge(A)-1) do
2      einzusortierender_wert = A[i]
3      j = i
4      while (j > 0) and (A[j-1] > einzusortierender_wert) do
5           A[j] = A[j - 1]
6           j = j − 1
7      end while
8      A[j] = einzusortierender_wert
9 end for
*/

fn sort_impl(list: &mut Vec<i64>) -> Vec<i64> {
    for i in 0..list.len() {
        let value: i64 = list[i];
        let mut j: usize = i;

        while j > 0 && list[j - 1] > value {
            list[j] = list[j - 1];
            j = j - 1;
        }
        list[j] = value;
    }
    return list.to_vec();
}

#[pyfunction]
fn sort(list_obj: PyObject) -> PyResult<PyObject> {
    // Py interpreter
    let gil = Python::acquire_gil();
    let py = gil.python();

    // Cast to list
    let list = list_obj.cast_as::<PyList>(py).unwrap();

    let mut vec: Vec<i64> = Vec::new();
    for i in 0..list.len() {
        // Get object from list
        let any: &PyAny = list.get_item(isize::try_from(i).unwrap());

        // Convert to PyLong (This will fail with non-number lists)
        let py_long: &PyLong = any.try_into().unwrap();

        // extract native rust value
        vec.insert(i, py_long.extract().unwrap());
    }

    return Ok(sort_impl(&mut vec).to_object(py));
}


/// This module is a python module implemented in Rust.
#[pymodule]
fn insertionsort_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    return m.add_wrapped(wrap_pyfunction!(sort));
}
