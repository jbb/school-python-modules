# How to run the example

Requirements:
 * Python virtualenv
 * rust nightly

Compile and install to virtualenv:
`./setup.sh install`

enter virtualenv:
`source ./setup.sh env`

Run examples:
`./tests.py`
This will generate a stats.csv
